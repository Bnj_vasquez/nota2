package com.example.nota2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private EditText ingresar;
    private Button buscar;
    private TextView ingredientes;
    private TextView nutricional;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.ingresar = (EditText) findViewById(R.id.editex);
        this.buscar = (Button) findViewById(R.id.buscar);
        this.ingredientes = (TextView) findViewById(R.id.ingredientes);
        this.nutricional = (TextView) findViewById(R.id.nutricional);
        this.buscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String comida= buscar.getText().toString();
                String url ="https://api.edamam.com/search?q="+comida+"&app_id=e40b1536&app_key=110731116d5b8ee1d55ed3638ef95af9&from=0&to=3&calories=591-722&health=alcohol-free";
                StringRequest solicitud = new StringRequest(
                        Request.Method.GET, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject respuestaJSON = new JSONObject(response);
                            ArrayList<String> nombre = new ArrayList<>();
                            JSONArray recetas = respuestaJSON.getJSONArray("hist");
                            JSONObject ingredientes = recetas.getJSONObject(0);
                            JSONObject recii = ingredientes.getJSONObject("recipe");
                            JSONArray ingret = recii.getJSONArray("ingredients");
                            for (int i = 0; i < ingret.length(); i++) {
                                JSONObject elemento = ingret.getJSONObject(i);
                                nombre.add(elemento.getString("text"));
                                ingredientes.setText("Estos son los Ingredientes: " + nombre);
                            }
                            ArrayList<String> informacion = new ArrayList<>();
                            JSONArray nutri = recii.getJSONArray("digest");
                            for (int i = 0; i < nutri.length(); i++) {
                                JSONObject objeto = nutri.getJSONObject(i);
                                informacion.add(objeto.getString("label"));
                                nutricional.setText("Informacion Nutricional: " + informacion);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // Algo fallo
                            }
                        }
                );
                RequestQueue listaEspera = Volley.newRequestQueue(getApplicationContext());
                listaEspera.add(solicitud);

            }
        });

    }
}
